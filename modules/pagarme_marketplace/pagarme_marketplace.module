<?php

/**
 * @file
 * Contains pagarme_marketplace.module.
 */

use Drupal\Core\Link;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\pagarme\Helpers\PagarmeUtility;

/**
 * Implements hook_help().
 */
function pagarme_marketplace_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the pagarme_marketplace module.
    case 'help.page.pagarme_marketplace':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Pagar.me - Marketplace') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_inline_entity_form_entity_form_alter().
 */
function pagarme_marketplace_inline_entity_form_entity_form_alter(&$entity_form, &$form_state) {
  if ($entity_form ['#entity_type'] == 'commerce_product_variation') {
    $entity_form['#element_validate'][] = 'pagarme_marketplace_inline_entity_product_form_entity_validate';
  }
}

/**
 * Custom validation callback for pagarme_marketplace_inline_entity_product_form_entity_validate().
 */
function pagarme_marketplace_inline_entity_product_form_entity_validate(&$entity_form, &$form_state) {
  if ($entity_form['#entity']->id()) {
    $values = $form_state->getValue($entity_form['#parents']);
    $new_price = current($values['price'])['number'];
    $new_price = PagarmeUtility::amountDecimalToInt($new_price);
    $product_active = boolval($values['status']['value']);
    if ($product_active) {
      $product_variation_id = $entity_form['#entity']->id();
      $database = \Drupal::database();
      $query = $database->select('pagarme_splits', 'splits');
      $query->addField('splits', 'split_id', 'split_id');
      $query->addField('splits', 'split_type', 'split_type');
      $query->addField('splits', 'amount', 'amount');
      $query->addField('splits', 'company', 'company');
      $query->condition('product_variation_id', $product_variation_id);
      $query->condition('status', 1);
      $splits = $query->execute()->fetchAll();

      $links_error = [];
      $i = 1;
      foreach ($splits as $split) {
        if ($split->split_type == 'amount') {
          $total_amount = intval($split->amount);
          $query = $database->select('pagarme_split_rules', 'rules');
          $query->addField('rules', 'amount', 'amount');
          $query->condition('split_id', $split->split_id);
          $rules = $query->execute()->fetchAll();

          foreach ($rules as $rule) {
            $total_amount += intval($rule->amount);
          }
          if ($new_price !== $total_amount) {
            $url = Url::fromRoute(
                'pagarme_marketplace.company_split_rules_edit', 
                [
                  'company' => $split->company,
                  'product_variation_id' => $product_variation_id, 
                  'split_id' =>  $split->split_id,
                  'op' => 'edit'
                ]
            );
            $url->setOptions(array(
              'attributes' => array(
                'target' => '_blank'
              )
            ));
            $link_text = t('split') . $i++;
            $links_error[] = Link::fromTextAndUrl($link_text, $url)->toString();
          }
        }
      }

      if (count($links_error)) {
        $parents_path = implode('][', $entity_form['#parents']);
        $form_state->setErrorByName($parents_path . '][price', t('Preço: existem regras de divisões inválidas para o preço informado, para poder salvar o produto é necessário desativa-las clincando nos links a seguir ou salvar o produto com status inativo: Links: ' . implode(', ', $links_error)));
      }
    }
  }
}